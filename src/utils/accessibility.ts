export function isElementFocused(element: HTMLElement) {
    return element === document.activeElement;
}
